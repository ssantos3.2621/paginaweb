<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Webster - Responsive Multi-purpose HTML5 Template" />
<meta name="author" content="potenzaglobalsolutions.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Webster - Responsive Multi-purpose HTML5 Template</title>

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico" />

<!-- Plugins -->
<link rel="stylesheet" type="text/css" href="css/plugins-css.css" />

<!-- revoluation -->
<link rel="stylesheet" type="text/css" href="css/settings.css" media="screen" />

<!-- Typography -->
<link rel="stylesheet" type="text/css" href="css/typography.css" />

<!-- Shortcodes -->
<link rel="stylesheet" type="text/css" href="css/shortcodes/shortcodes.css" />

<!-- Style -->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<!-- magazine -->
<link rel="stylesheet" type="text/css" href="css/magazine-02.css" />

<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="css/responsive.css" />

<!-- Style customizer -->
<link rel="stylesheet" type="text/css" href="css/skins/skin-orange.css" data-style="styles"/>

</head>

<body>

<div class="wrapper">

<!--=================================
 preloader -->

<div id="pre-loader">
    <img src="images/pre-loader/loader-02.svg" alt="">
</div>

<!--=================================
 preloader -->

<!--=================================
 header -->

<header id="header" class="header light logo-center magazine-header">
 <!--=================================
 mega menu -->

<div class="menu">
  <!-- menu start -->
   <nav id="menu" class="mega-menu">
    <!-- menu list items container -->
    <section class="menu-list-items">
     <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12 d-flex align-items-center">
        <div class="mt-0">
        <!-- menu logo -->
          <ul class="menu-logo">
              <li>
                  <a href="index-01.html"><img id="logo_img" src="images/logo.png" alt="logo"> </a>
              </li>
          </ul>
         </div>
        <!-- menu links -->
        <div class="menu-bar ml-auto">
         <ul class="menu-links">

            <li><a href="">INICIO</a>
                <!-- drop down full width -->

            </li>
            <li><a href="">NOSOTROS</a>
                <!-- drop down full width -->

            </li>
            <li><a href="">SERVICIOS</a>
                <!-- drop down multilevel  -->

            </li>
            <li><a href="">PORTAFOLIO</a>
                <!-- drop down multilevel  -->

            </li>
            <li><a href="">CONTACTO</a>
            <!-- drop down multilevel  -->

             </li>
            <li><a href="">BLOG</a>
              <!-- drop down full width -->

            </li>
        </ul>
        </div>
       </div>
      </div>
     </div>
    </section>
   </nav>
  <!-- menu end -->
 </div>
</header>

<!--=================================
 header -->


<!--=================================
 banner -->

 <section class="white-bg o-hidden">
   <div class="container-fluid p-0">
    <div class="owl-carousel bottom-right-dots" data-nav-dots="true" data-items="4" data-md-items="4" data-sm-items="2" data-xs-items="1" data-xx-items="1" data-space="0">
     <div class="item">
       <div class="blog-overlay">
         <div class="blog-image">
            <img class="img-fluid" src="images/banner/01.jpg" alt="">
         </div>
         <div class="blog-name">
           <a class="tag" href="#"> Fashion </a>
            <h4 class="mt-15 text-white"><a href="#">Does your life lack meaning</a></h4>
            <span class="text-white">By adimn / <a href="#">Business</a></span>
        </div>
      </div>
     </div>
     <div class="item">
       <div class="blog-overlay">
         <div class="blog-image">
            <img class="img-fluid" src="images/banner/02.jpg" alt="">
         </div>
         <div class="blog-name">
           <a class="tag" href="#"> Style </a>
            <h4 class="mt-15 text-white"><a href="#">Supercharge your motivation</a></h4>
            <span class="text-white">By adimn / <a href="#">Business</a></span>
        </div>
      </div>
     </div>
     <div class="item">
       <div class="blog-overlay">
         <div class="blog-image">
            <img class="img-fluid" src="images/banner/03.jpg" alt="">
         </div>
         <div class="blog-name">
           <a class="tag" href="#"> Lookbook </a>
            <h4 class="mt-15 text-white"><a href="#">Helen keller a teller seller</a></h4>
            <span class="text-white">By adimn / <a href="#">Business</a></span>
        </div>
      </div>
     </div>
     <div class="item">
       <div class="blog-overlay">
         <div class="blog-image">
            <img class="img-fluid" src="images/banner/04.jpg" alt="">
         </div>
         <div class="blog-name">
           <a class="tag" href="#"> Trend </a>
            <h4 class="mt-15 text-white"><a href="#">The other virtues practice</a></h4>
            <span class="text-white">By adimn / <a href="#">Business</a></span>
        </div>
      </div>
     </div>
   </div>
  </div>
 </section>

<!--=================================
 banner -->


<!--=================================
 blog -->

 <section class="page-section-ptb white-bg o-hidden">
   <div class="container">
      <div class="row">
        <div class="col-lg-9">
          <div class="row">
          <div class="col-md-12">
            <div class="section-title line-dabble">
              <h4 class="title">Today's Highlights</h4>
            </div>
         </div>
          <div class="col-md-6">
           <div class="blog-overlay">
             <div class="blog-image">
                <img class="img-fluid" src="images/03.jpg" alt="">
             </div>
             <div class="blog-name">
               <a class="tag" href="#"> Clothes </a>
                <h4 class="mt-15 text-white"><a href="#">You will begin to realise</a></h4>
                <span class="text-white">By adimn / <a href="#">Business</a></span>
            </div>
          </div>
         </div>
         <div class="col-md-6 xs-mt-30">
           <div class="blog-overlay">
             <div class="blog-image">
                <img class="img-fluid" src="images/04.jpg" alt="">
             </div>
             <div class="blog-name">
               <a class="tag" href="#"> Mackup </a>
                <h4 class="mt-15 text-white"><a href="#">Step out on to the path</a></h4>
                <span class="text-white">By adimn / <a href="#">Business</a></span>
            </div>
          </div>
         </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="sidebar-widget mt-30">
             <div class="recent-post medium clearfix">
             <div class="recent-post-info">
              <h5><a href="#">Auctor aliquet Aenean auctor alique Nibh vel velit</a> </h5>
              <span><i class="fa fa-calendar-o"></i> September 30, 2018</span>
             </div>
            </div>
          </div>
         </div>
         <div class="col-md-6">
           <div class="sidebar-widget mt-30">
            <div class="recent-post medium clearfix">
               <div class="recent-post-info">
                <h5><a href="#">Aliquet Aenean auctor alique Nibh vel velit auctor </a> </h5>
                <span><i class="fa fa-calendar-o"></i> September 30, 2018</span>
               </div>
              </div>
            </div>
         </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="sidebar-widget mt-30">
             <div class="recent-post medium clearfix">
             <div class="recent-post-info">
              <h5><a href="#">Aenean auctor alique Nibh vel velit auctor aliquet </a> </h5>
              <span><i class="fa fa-calendar-o"></i> September 30, 2018</span>
             </div>
            </div>
          </div>
         </div>
         <div class="col-md-6">
           <div class="sidebar-widget mt-30">
            <div class="recent-post medium clearfix">
               <div class="recent-post-info">
                <h5><a href="#">Auctor alique Nibh vel velit auctor aliquet Aenean </a> </h5>
                <span><i class="fa fa-calendar-o"></i> September 30, 2018</span>
               </div>
              </div>
            </div>
         </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="add-banner mt-30 mb-30">
              <a href=""> <img class="img-fluid" src="images/banner-02.jpg" alt=""></a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-7">
            <div class="section-title line-dabble">
              <h4 class="title">Latest Posts</h4>
            </div>
           <div class="owl-carousel bottom-right-dots" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1" data-space="0">
             <div class="item">
               <div class="blog-overlay">
                 <div class="blog-image">
                    <img class="img-fluid" src="images/01.jpg" alt="">
                 </div>
                 <div class="blog-name">
                   <a class="tag" href="#"> Style </a>
                    <h4 class="mt-15 text-white"><a href="#">You continue doing what</a></h4>
                    <span class="text-white">By adimn / <a href="#">Business</a></span>
                </div>
              </div>
             </div>
             <div class="item">
               <div class="blog-overlay">
                 <div class="blog-image">
                    <img class="img-fluid" src="images/02.jpg" alt="">
                 </div>
                 <div class="blog-name">
                   <a class="tag" href="#"> Lookbook </a>
                    <h4 class="mt-15 text-white"><a href="#">Walk out 10 years into</a></h4>
                    <span class="text-white">By adimn / <a href="#">Business</a></span>
                </div>
              </div>
             </div>
           </div>
          </div>
        <div class="col-md-5">
          <div class="section-title line-dabble xs-mt-30">
            <h4 class="title">Most Trending</h4>
          </div>
          <div class="sidebar-widget">
           <div class="recent-post medium clearfix">
              <div class="recent-post-image">
                <img class="img-fluid" src="images/blog/01.jpg" alt="">
              </div>
             <div class="recent-post-info">
              <a href="#">Auctor aliquet Aenean Nibh vel velit </a>
              <span><i class="fa fa-calendar-o"></i> September 15, 2018</span>
             </div>
            </div>
            <div class="recent-post medium clearfix">
              <div class="recent-post-image">
                <img class="img-fluid" src="images/blog/02.jpg" alt="">
              </div>
             <div class="recent-post-info">
              <a href="#">Aliquet Aenean Nibh vel velit auctor </a>
              <span><i class="fa fa-calendar-o"></i> September 19, 2018</span>
             </div>
            </div>
            <div class="recent-post medium clearfix">
              <div class="recent-post-image">
                <img class="img-fluid" src="images/blog/03.jpg" alt="">
              </div>
             <div class="recent-post-info">
              <a href="#">Aenean Nibh vel velit auctor aliquet </a>
              <span><i class="fa fa-calendar-o"></i> September 25, 2018</span>
             </div>
            </div>
            <div class="recent-post medium clearfix">
              <div class="recent-post-image">
                <img class="img-fluid" src="images/blog/04.jpg" alt="">
              </div>
             <div class="recent-post-info">
              <a href="#">Velit auctor aliquet Aenean Nibh vel </a>
              <span><i class="fa fa-calendar-o"></i> September 30, 2018</span>
             </div>
            </div>
          </div>
         </div>
        </div>
       </div>
        <div class="col-lg-3 sm-mt-30">
              <div class="row">
                <div class="col-md-12">
                  <div class="section-title line-dabble">
                    <h4 class="title">Hot News</h4>
                  </div>
               </div>
                <div class="col-lg-6 col-md-3 col-sm-6">
                  <div class="blog blog-simple text-left mb-20">
                     <div class="blog-image">
                        <img class="img-fluid" src="images/05.jpg" alt="">
                     </div>
                     <div class="blog-name mt-20">
                      <h6 class="mt-15"><a href="#">Motivation In Life</a></h6>
                     <div class="admin">
                         <span>By adimn / <a href="#">Trend</a></span>
                       </div>
                    </div>
                </div>
              </div>
               <div class="col-lg-6 col-md-3 col-sm-6">
               <div class="blog blog-simple text-left mb-20">
                   <div class="blog-image">
                      <img class="img-fluid" src="images/06.jpg" alt="">
                   </div>
                   <div class="blog-name mt-20">
                    <h6 class="mt-15"><a href="#">Are You Famous Or Focused</a></h6>
                   <div class="admin">
                       <span>By adimn / <a href="#">Business</a></span>
                     </div>
                  </div>
               </div>
              </div>
              <div class="col-lg-6 col-md-3 col-sm-6">
                <div class="blog blog-simple text-left xs-mb-20">
                     <div class="blog-image">
                        <img class="img-fluid" src="images/07.jpg" alt="">
                     </div>
                     <div class="blog-name mt-20">
                      <h6 class="mt-15"><a href="#">Burning Desire Golden Key</a></h6>
                     <div class="admin">
                         <span>By adimn / <a href="#">Clothes</a></span>
                       </div>
                     </div>
                 </div>
                </div>
                <div class="col-lg-6 col-md-3 col-sm-6">
                <div class="blog blog-simple text-left xs-mb-20">
                     <div class="blog-image">
                        <img class="img-fluid" src="images/08.jpg" alt="">
                     </div>
                     <div class="blog-name mt-20">
                      <h6 class="mt-15"><a href="#">Hypnotherapy For Motivation </a></h6>
                     <div class="admin">
                         <span>By adimn / <a href="#">Mackup</a></span>
                       </div>
                     </div>
                 </div>
                </div>
         </div>
          <div class="section-title line-dabble mt-30">
            <h4 class="title">Connect with us</h4>
          </div>
          <div class="social-big-magazine">
             <ul>
              <li class="facebook"><a href="#"><i class="fa fa-facebook"></i> <span>859 Fans</span></a> </li>
              <li class="pinterest"><a href="#"><i class="fa fa-pinterest"></i> <span> followers</span> </a></li>
              <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i> <span>Contacts</span> </a></li>
              <li class="youtube"><a href="#"><i class="fa fa-youtube"></i> <span>Subscribers</span> </a></li>
              <li class="twitter"><a href="#"><i class="fa fa-twitter"></i> <span>followers</span> </a></li>
              <li class="dribbble"><a href="#"><i class="fa fa-dribbble"></i> <span>followers</span> </a></li>
             </ul>
           </div>
          <div class="add mt-30">
            <a href=""></a> <img class="img-fluid" src="images/banner.jpg" alt=""></div>
             <div class="sidebar-widget mt-30">
           <div class="widget-newsletter">
            <div class="newsletter-icon">
              <i class="fa fa-envelope-o"></i>
            </div>
            <div class="newsletter-content">
              <i>Enter your e-mail and subscribe to our newsletter. </i>
            </div>
            <div class="newsletter-form mt-20">
                <div class="form-group">
                    <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Name">
                </div>
               <a class="button btn-block" href="#">Submit</a>
            </div>
          </div>
         </div>
        </div>
        </div>
      <div class="page-section-pt">
      <div class="row">
        <div class="col-md-12">
          <div class="section-title line-dabble">
            <h4 class="title">Video Gallery</h4>
          </div>
       </div>
      <div class="col-md-4">
         <div class="blog-box blog-2 blog-border">
            <div class="popup-video-image border-video popup-gallery">
              <img class="img-fluid" src="images/video/01.jpg" alt="">
                <a class="popup-youtube" href="https://www.youtube.com/watch?v=LgvseYYhqU0"> <i class="fa fa-play"></i> </a>
                <div class="video-attribute">
                  <span class="length">01:06</span>
                  <span class="quality">HD</span>
                </div>
            </div>
            <div class="blog-info">
              <span class="tag"><a href="#">Business</a></span>
              <h3 class="mt-20"> <a href="#"> Does your life</a></h3>
              <span>By adimn / <i>4 months ago </i></span>
         </div>
        </div>
      </div>
      <div class="col-md-4">
         <div class="blog-box blog-2 blog-border">
            <div class="popup-video-image border-video popup-gallery">
              <img class="img-fluid" src="images/video/02.jpg" alt="">
                <a class="popup-youtube" href="https://www.youtube.com/watch?v=LgvseYYhqU0"> <i class="fa fa-play"></i> </a>
                <div class="video-attribute">
                  <span class="length">03:20</span>
                  <span class="quality">HD</span>
                </div>
            </div>
            <div class="blog-info">
              <span class="tag"><a href="#">Style</a></span>
              <h3 class="mt-20"> <a href="#"> Supercharge your</a></h3>
              <span>By adimn / <i>6 months ago </i></span>
         </div>
        </div>
      </div>
      <div class="col-md-4">
         <div class="blog-box blog-2 blog-border">
            <div class="popup-video-image border-video popup-gallery">
              <img class="img-fluid" src="images/video/03.jpg" alt="">
                <a class="popup-youtube" href="https://www.youtube.com/watch?v=LgvseYYhqU0"> <i class="fa fa-play"></i> </a>
                <div class="video-attribute">
                  <span class="length">03:45</span>
                  <span class="quality">HD</span>
                </div>
            </div>
            <div class="blog-info">
              <span class="tag"><a href="#">Lookbook</a></span>
              <h3 class="mt-20"> <a href="#">Helen keller a</a></h3>
              <span>By adimn / <i>8 months ago </i></span>
         </div>
        </div>
      </div>
     </div>
    </div>
   </div>
 </section>

<!--=================================
 blog -->

<!--=================================
 footer -->

<footer class="footer page-section-pt black-bg">
 <div class="container">
  <div class="row">
      <div class="col-lg-3 col-sm-6 sm-mb-30">
      <div class="footer-hedding">
        <h6 class="text-white mb-30 mt-10 text-uppercase">About us</h6>
           <div class="clearfix">
             <h4 class="text-gray">Multipurpose HTML5 template</h4>
             <p class="mt-20">Webster is the most enticing, creative, modern and multipurpose Premium HTML5 Template suitable for any business or corporate websites.</p>
           </div>
      </div>
    </div>
    <div class="col-lg-3 col-sm-6 sm-mb-30 xs-mb-30">
      <h6 class="text-white mb-30 mt-10 text-uppercase">Subscribe to Our Newsletter</h6>
      <p>Sign Up to our Newsletter to get the latest news and offers.</p>
       <div class="footer-Newsletter">
        <div id="mc_embed_signup_scroll">
          <form action="" method="POST" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate">
           <div id="msg"> </div>
            <div id="mc_embed_signup_scroll_2">
              <input id="mce-EMAIL" class="form-control" type="text" placeholder="Email address" name="email1" value="">
            </div>
            <div id="mce-responses" class="clear">
              <div class="response" id="mce-error-response" style="display:none"></div>
              <div class="response" id="mce-success-response" style="display:none"></div>
              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
              <div style="position: absolute; left: -5000px;" aria-hidden="true">
                  <input type="text" name="b_b7ef45306f8b17781aa5ae58a_6b09f39a55" tabindex="-1" value="">
              </div>
              <div class="clear">
                <button type="submit" name="submitbtn" id="mc-embedded-subscribe" class="button button-border mt-20 form-button">  Get notified </button>
              </div>
            </form>
          </div>
          </div>
       </div>
    <div class="col-lg-3 col-sm-6 xs-mb-30">
      <div class="footer-hedding">
        <h6 class="text-white mb-30 mt-10 text-uppercase">recent work</h6>
         <div class="footer-gallery clearfix">
            <ul>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/01.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/02.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/03.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/04.gif" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/05.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/06.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/07.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/08.gif" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/09.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/10.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/01.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/02.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/03.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/05.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/06.jpg" alt=""> </a> </li>
              <li> <a href="#"> <img class="img-fluid" src="images/portfolio/07.jpg" alt=""> </a> </li>
            </ul>
          </div>
      </div>
    </div>
    <div class="col-lg-3 col-sm-6 xs-mb-30">
      <h6 class="text-white mb-30 mt-10 text-uppercase">Popular tags</h6>
       <div class="footer-tags">
         <ul>
          <li><a href="#">Bootstrap</a></li>
          <li><a href="#">HTML5</a></li>
          <li><a href="#">Wordpress</a></li>
          <li><a href="#">CSS3</a></li>
          <li><a href="#">Creative</a></li>
          <li><a href="#">Multipurpose</a></li>
          <li><a href="#">Bootstrap</a></li>
          <li><a href="#">HTML5</a></li>
          <li><a href="#">Wordpress</a></li>
        </ul>
      </div>
      </div>
       </div>
      <div class="footer-widget mt-60 ">
        <div class="row">
          <div class="col-lg-6 col-md-6">
           <p class="mt-15"> All Rights Reserved </p>
          </div>
          <div class="col-lg-6 col-md-6 text-left text-md-right">
            <div class="footer-social mt-10">
              <ul>
                 <li class="list-inline-item"><a href="#">Terms &amp; Conditions </a> &nbsp;&nbsp;&nbsp;|</li>
                 <li class="list-inline-item"><a href="#">API Use Policy </a> &nbsp;&nbsp;&nbsp;|</li>
                 <li class="list-inline-item"><a href="#">Privacy Policy </a> </li>
              </ul>
              </div>
          </div>
        </div>
      </div>
  </div>
</footer>

<!--=================================
 footer -->

</div>

<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-angle-up"></i> <span>TOP</span></a></div>

<!--=================================
 jquery -->

<!-- jquery -->
<script src="js/jquery-3.4.1.min.js"></script>

<!-- plugins-jquery -->
<script src="js/plugins-jquery.js"></script>

<!-- plugin_path -->
<script>var plugin_path = 'js/';</script>

<!-- custom -->
<script src="js/custom.js"></script>



</body>
</html>
